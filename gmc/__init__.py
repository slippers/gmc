from .gmc import GMC
from .decorator import gmc_connection

name = "gmc"
__version__ = '1.0.0'
__author__ = 'kirk erickson'
